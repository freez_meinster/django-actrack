.. django-actrack documentation master file

django-actrack
==============

|copyright| 2014-2018 Thomas Khyn

``django-actrack`` is an activity tracker for the Django framework. It enables
recording any activity by any actor, relative to any number of targets or
related objects, with or without additional data. The activity can then be
retrieved through feeds associated to any instance linked to any action(s).

It has been tested with Django 1.11 and 2.0 and the latest minor versions
of Python 2 and 3 (Django 2.0 only supports Python 3).

If you like django-actrack and are looking for a way to thank its creator and/or
encourage future development, here is a BTC or BCH donation address:
``1EwENyR8RV6tMc1hsLTkPURtn5wJgaBfG9``.

Documentation contents:

.. toctree::
   :maxdepth: 2

   quick_start
   advanced
   settings
   api


.. warning::

   This documentation is a work in progress. Some features may be undocumented,
   or only lightly documented. It may be necessary to have a look at the
   `source code`_ for more details on some features.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. |copyright| unicode:: 0xA9

.. _`source code`: https://bitbucket.org/tkhyn/django-actrack/src
